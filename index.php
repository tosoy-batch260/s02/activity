<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>S02: Repetition Control Structures & Array Manipulation</title>
</head>
<body>
    <h1>Divisible by 5</h1>
    <p><?= printDivisibleBy5(); ?></p>

    <h1>Array Manipulation</h1>
    <?php array_push($students, 'Jhepoy Dizon'); ?>
    <p><?= var_dump($students); ?></p>
    <p><?= count($students); ?></p>

    <?php array_push($students, 'Mang Boy'); ?>
    <p><?= var_dump($students); ?></p>
    <p><?= count($students); ?></p>

    <?php array_shift($students); ?>
    <p><?= var_dump($students); ?></p>
    <p><?= count($students); ?></p>
</body>
</html>